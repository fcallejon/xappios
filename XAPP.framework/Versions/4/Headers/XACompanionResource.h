//
//  XACompanionResource.h
//  XAPP
//
//  Created by Michael Myers on 4/18/16.
//  Copyright © 2016 XAPPmedia. All rights reserved.
//

#import "XALoading.h"
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 *  The companion resource is the primary display resource for the companion ad.
 *
 *  See [DAAST 3.2.2.2](http://www.iab.com/wp-content/uploads/2015/12/DAAST_1.1-3.pdf) for
 * information on the companion resource.
 */
@protocol XACompanionResource <XALoading, NSCoding>

/**
 *  The image that is displayed on the companion.
 *
 *  This is typically used for displaying on external devices and screens while the ad plays.
 */
@property (nullable, readonly) UIImage *image;

/**
 *  The URL for the resource.
 */
@property (nonnull, readonly) NSURL *url;

@end
