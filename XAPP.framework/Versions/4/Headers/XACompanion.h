//
//  XACompanion.h
//  XAPP
//
//  Created by Michael Myers on 7/14/16.
//  Copyright © 2016 XAPPmedia. All rights reserved.
//

#import <Foundation/Foundation.h>

@class XASettings, XAAction;
@protocol XACompanionResource, XACompanionDelegate;

@protocol XACompanion <NSObject>

/**
 *  Action performed when the companion is touched.
 *
 *  This is only used if the companion cannot provide clickthroughs
 *  on its own, such as IFrameResource (not currently supported).
 *
 *  See [DAAST 3.2.2.7](http://www.iab.com/wp-content/uploads/2015/12/DAAST_1.1-3.pdf) for companion
 * tracking details.
 *  See [DAAST 3.1.5.2](http://www.iab.com/wp-content/uploads/2015/12/DAAST_1.1-3.pdf) for companion
 * click tracking details.
 *
 */
@property (nullable, readonly) XAAction *clickThrough;

/**
 *  An array of URLs that are called when the companion is clicked.
 *
 *  See [DAAST 3.2.2.7](http://www.iab.com/wp-content/uploads/2015/12/DAAST_1.1-3.pdf) for companion
 * tracking details.
 *  See [DAAST 3.1.5.2](http://www.iab.com/wp-content/uploads/2015/12/DAAST_1.1-3.pdf) for companion
 * click tracking details.
 */
@property (nonnull, readonly) NSArray *clickTracking;

/**
 *  An array of tracking events for the companion.
 *
 *  The only event available for tracking is the `creativeView` event.
 *
 *  See [DAAST 3.2.2.7](http://www.iab.com/wp-content/uploads/2015/12/DAAST_1.1-3.pdf) for companion
 * tracking details.
 */
@property (nonnull, readonly) NSArray *trackingEvents;

/**
 *  The companion resource.
 *
 *  See [DAAST 3.2.2.2](http://www.iab.com/wp-content/uploads/2015/12/DAAST_1.1-3.pdf) for companion
 * resource types.
 */
@property (nonnull, readonly) NSObject<XACompanionResource> *companionResource;

/**
 *  The settings for the companion
 */
@property (nonnull) XASettings *settings;

/**
 *  If the companion is visible to the user.
 */
@property (readonly) BOOL visible;

/**
 *  Delegate object to receive companion events.
 */
@property (nonatomic, retain, nullable, readwrite) NSObject<XACompanionDelegate> *delegate;

/**
 *  Create a companion with a resource and all possible tracking.
 *
 *  @param clickThrough      Action that is performed when the companion is touched
 *  @param clickTracking     An array of tracking URLs that are fired when the companion is touched
 *  @param trackingEvents    An array of tracking events for the companion
 *  @param companionResource Companion resource
 *  @param settings          The settings for the companion
 *
 *  @return New companion view
 */
+ (nonnull instancetype)companionWithClickThrough:(nullable XAAction *)clickThrough
                                    clickTracking:(nullable NSArray *)clickTracking
                                   trackingEvents:(nullable NSArray *)trackingEvents
                                companionResource:(nonnull NSObject<XACompanionResource> *)companionResource
                                         settings:(nullable XASettings *)settings;

/**
 *  Create a new companion with click through, tracking, and companion resource.
 *
 *  @param clickThrough      Action that is performed when the companion is touched
 *  @param clickTracking     An array of tracking URLs that are fired when the companion is touched
 *  @param trackingEvents    An array of tracking events for the companion
 *  @param companionResource Companion resource
 *  @param settings          The settings for the companion
 *
 *  @return New companion view
 */
- (nonnull instancetype)initWithClickThrough:(nullable XAAction *)clickThrough
                               clickTracking:(nullable NSArray *)clickTracking
                              trackingEvents:(nullable NSArray *)trackingEvents
                           companionResource:(nonnull NSObject<XACompanionResource> *)companionResource
                                    settings:(nullable XASettings *)settings;

@end
