//
//  XAAdResult.h
//  XappAds
//
//  Created by the developers at XAPPmedia in beautiful Washington, DC
//  Copyright (c) 2015 XAPPmedia. All rights reserved.
//
//  Your use of the XAPP SDK is governed by the XAPP SDK Terms of Service and License Agreement
//  http://xappmedia.com/xapp-sdk/sdk-terms/
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class XAAction;
@class XAAdvertisement;

FOUNDATION_EXPORT NSString *__nonnull const XAResultCancellationTypeStringProgrammatic;
FOUNDATION_EXPORT NSString *__nonnull const XAResultCancellationTypeStringCancelTouched;
FOUNDATION_EXPORT NSString *__nonnull const XAResultCancellationTypeStringRemoteControlEvent;

/**
 *  Enum for the different ways in which an ad can be cancelled.
 */
typedef NS_ENUM(NSInteger, XAResultCancellationType) {
    /**
     *  The default cancellation type, no cancellation occured.
     */
    XAResultCancellationTypeNone,
    /**
     *  The ad was cancelled programmtically with [XAPP cancelAd:]
     */
    XAResultCancellationTypeProgrammatic,
    /**
     *  The ad was cancelled by the user touching the cancel button
     */
    XAResultCancellationTypeCancelTouched,
    /**
     *  The ad was cancelled by the user touching a fast-forward/skip button through the lock screen
     * or an external peripheral.
     */
    XAResultCancellationTypeRemoteControlEvent
};

FOUNDATION_EXPORT NSString *__nonnull const XAAdResultCallTypeServerInitiated;
FOUNDATION_EXPORT NSString *__nonnull const XAAdResultCallTypeUserInitiated;
FOUNDATION_EXPORT NSString *__nonnull const XAAdResultCallTypeCallInitiated;

/**
 *  The XAAdResult provides detailed information about the playback of an advertisement.
 */
@interface XAResult : NSObject <NSCoding, NSCopying>

+ (nonnull XAResult *)resultForAdvertisement:(nonnull XAAdvertisement *)advertisement;

- (nonnull NSDictionary *)serialize;

///---------------------------------------------------------------------------------------
/// @name Advertisement Information
///---------------------------------------------------------------------------------------

/**
 *  The ID of the advertisement that was played
 */
@property (nonnull, nonatomic, retain) NSString *adID;

/**
 *  The request key that was used to retrieve the ad
 */
@property (nonnull, nonatomic, retain) NSString *requestKey;

/**
 *  The session key for the current active listening session
 */
@property (nonnull, nonatomic, retain) NSString *sessionKey;

/**
 *  The URL for where the advertisement originated.
 */
@property (nonnull, nonatomic, retain) NSURL *server;

///---------------------------------------------------------------------------------------
/// @name Playback Information
///---------------------------------------------------------------------------------------

/**
 *  Advertisement start time as an ISO-8601 string
 */
@property (nonnull, nonatomic, retain) NSString *startPlay;
/**
 *  Advertisement end time as an ISO-8601 string
 */
@property (nonnull, nonatomic, retain) NSString *endPlay;
/**
 *  The ad playback was succesful, if `NO` then look for an error
 */
@property (nonatomic, assign) BOOL success;

/**
 *  The error, if one did occur, for the advertisement playback.
 *
 *  @see XAErrorCode
 */
@property (nullable, nonatomic, retain) NSError *error;

/**
 *  Boolean for if the user responded by voice to an action
 *
 *  @since 3.11.0
 */
@property (nonatomic, assign) BOOL voiceClick;

/**
 *  The recognized action, nil if no action was recognized.
 */
@property (nullable, nonatomic, retain) XAAction *recognizedAction;

/**
 *  If an audio action was triggered.
 */
@property BOOL audioAction;

/**
 *  Boolean for if the ad image was touched by the user.
 *
 *  @since 3.11.0
 */
@property (nonatomic, assign) BOOL touchClick;

/**
 *  Coordinates for where the user touched the screen.  The last touch is reported and earlier
 * touches are overwritten.
 *
 *  @since 3.11.0
 */
@property (nonatomic, assign) CGPoint touchClickLocation;

/**
 *  The action the was completed when the complementary companion was touched.
 */
@property (nullable, nonatomic, retain) XAAction *touchAction;

/**
 *  Completion of the action will be deferred until the application moves back to the foreground.
 */
@property (nullable, nonatomic, retain) XAAction *deferredAction;

/**
 *  If the advertisement was cancelled by either programmatically or the user clicked the 'X'.  See
 * cancellationType for more information on the cancellation.
 *
 *  @see cancellationType
 *  @see cancellationTime
 */
@property (nonatomic, assign) BOOL cancelled;

/**
 *  Describes method of cancellation, either XAAdResultCancellationTypeStringProgrammatic,
 * XAAdResultCancellationTypeStringCancelTouched,
 * XAAdResultCancellationTypeStringRemoteControlEvent.  See cancelled to check if the ad was
 * cancelled.
 *
 *  @see cancelled
 *  @see cancellationType
 *  @see cancellationTime
 */
@property (nullable, nonatomic, retain) NSString *cancellationTypeString;

/**
 *  How the ad was cancelled
 *
 *  @since 3.7.0
 *  @see cancelled
 *  @see cancellationTypeString
 */
@property XAResultCancellationType cancellationType;

/**
 *  Convenience method for converting the cancellationType to a human readable string, can be used
 * for debugging.
 *
 *  @since 3.7.0
 *  @see cancellationType
 *  @param cancellationType Cancellation type observed during ad playback
 *
 *  @return String representing the cancellationType
 */
+ (nullable NSString *)cancellationTypeToString:(XAResultCancellationType)cancellationType;

/**
 *  If the ad was cancelled, the cancellation time is the amount into audio playback that the ad was
 * cancelled.
 *
 *  @see cancelled
 *  @see cancellationType
 */
@property (nonatomic, assign) NSTimeInterval cancellationTime;

/**
 *  The image was dismissed through the close button.
 */
@property (nonatomic, assign) BOOL interstitialSkipped;

///---------------------------------------------------------------------------------------
/// @name Resource Load Diagnostic Information
///---------------------------------------------------------------------------------------

/**
 *  Time to complete voice recognition
 */
@property (nonatomic) NSTimeInterval recognitionTime;

/**
 *  Time to complete loading of the linear creative
 */
@property (nonatomic) NSTimeInterval linearLoadTime;

/**
 *  Time to complete loading of the companion creative.
 */
@property (nonatomic) NSTimeInterval companionLoadTime;

///---------------------------------------------------------------------------------------
/// @name Diagnostic Information
///---------------------------------------------------------------------------------------

/**
 *  The Audio Route used during playback
 */
@property (nonnull, nonatomic, retain) NSString *audioRoute;
/**
 *  The more specific audio route name used during playback
 */
@property (nonnull, nonatomic, retain) NSString *audioRouteName;

/**
 *  The connection type at the start of the ad
 */
@property (nonnull, nonatomic, retain) NSString *connectionType;

/**
 *  If the device was charging.
 */
@property BOOL deviceCharging;

/**
 *  If the app was in the background at the start of playback.
 */
@property BOOL backgroundedAtStart;

/**
 *  If the app was in the background when voice recognition occured.
 */
@property BOOL backgroundedAtVoiceRecognition;

/**
 *  If the app was in the background at the end of playback.
 */
@property BOOL backgrounded;

/**
 *  The email address to use for the action. Necessary for BUY and EMAIL action types
 */
@property (nonnull, nonatomic, retain) NSString *email;

@end
