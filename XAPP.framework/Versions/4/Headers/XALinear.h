//
//  XALinear.h
//  XAPP
//
//  Created by Michael Myers on 4/20/16.
//  Copyright © 2016 XAPPmedia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XAPlaying.h"

@class XASettings;
@class XALinearComponents;

/**
 *  A linear creative is any creative that has a dependency on time.  Typically, this is either
 * audio or video creative.
 *
 *  The XALinear class has no functionality on its own and is meant to be subclassed.  The class
 * implements XAPlaying, which
 *  facilitates how to control the creative's playback.
 *
 *  See [DAAST 3.2.1](http://www.iab.com/wp-content/uploads/2015/12/DAAST_1.1-3.pdf)
 */
@interface XALinear : NSObject <XAPlaying, NSCopying>

/**
 *  The duration of the creative.
 *
 *  See [DAAST 3.2.1.2](http://www.iab.com/wp-content/uploads/2015/12/DAAST_1.1-3.pdf)
 */
@property (readonly) NSTimeInterval duration;

/**
 *  The media files for the creative.
 *
 *  See [DAAST 3.2.1.3](http://www.iab.com/wp-content/uploads/2015/12/DAAST_1.1-3.pdf)
 */
@property (nonnull, readonly) NSArray *mediaFiles;

/**
 *  Tracking events for the linear
 *
 *  See [DAAST 3.2.1.6](http://www.iab.com/wp-content/uploads/2015/12/DAAST_1.1-3.pdf)
 */
@property (nonnull, readonly) NSArray *trackingEvents;

/**
 *  The ad interactions
 *
 *  See [DAAST 3.2.1.6](http://www.iab.com/wp-content/uploads/2015/12/DAAST_1.1-3.pdf)
 */
@property (nonnull, readonly) NSArray *adInteractions;

/**
 *  The settings for the linear.
 */
@property (nonatomic, readonly, nonnull) XASettings *settings;

/**
 *  Create a new linear based on the required elements.
 *
 *  @param duration       The duration of the linear
 *  @param mediaFiles     Mediafiles for the linear
 *  @param trackingEvents Tracking events that fire during playback
 *  @param adInteractions Interactions for the linear
 *
 *  @return A new linear creative
 */
+ (nonnull XALinear *)linearWithDuration:(NSTimeInterval)duration
                              mediaFiles:(nonnull NSArray *)mediaFiles
                          trackingEvents:(nonnull NSArray *)trackingEvents
                          adInteractions:(nonnull NSArray *)adInteractions;

/**
 *  Create a new linear based on the required elements.
 *
 *  @param duration       The duration of the linear
 *  @param mediaFiles     Mediafiles for the linear
 *  @param trackingEvents Tracking events that fire during playback
 *  @param adInteractions Interactions for the linear
 *  @param settings       Settings for linear playback
 *
 *  @return A new linear creative
 */
+ (nonnull XALinear *)linearWithDuration:(NSTimeInterval)duration
                              mediaFiles:(nonnull NSArray *)mediaFiles
                          trackingEvents:(nonnull NSArray *)trackingEvents
                          adInteractions:(nonnull NSArray *)adInteractions
                                settings:(nonnull XASettings *)settings;

/**
 *  Create a new linear based on the required elements.
 *
 *  @param duration       The duration of the linear
 *  @param mediaFiles     Mediafiles for the linear
 *  @param trackingEvents Tracking events that fire during playback
 *  @param adInteractions Interactions for the linear
 *  @param settings       Settings for linear playback
 *
 *  @return A new linear creative
 */
- (nonnull instancetype)initWithDuration:(NSTimeInterval)duration
                              mediaFiles:(nonnull NSArray *)mediaFiles
                          trackingEvents:(nonnull NSArray *)trackingEvents
                          adInteractions:(nonnull NSArray *)adInteractions
                                settings:(nonnull XASettings *)settings;

@end
