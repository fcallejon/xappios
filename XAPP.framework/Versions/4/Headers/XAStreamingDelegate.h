//
//  XAStreamingDelegate.h
//
//  Created by the developers at XAPPmedia in beautiful Washington, DC
//  Copyright (c) 2015 XAPPmedia. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Defines XAStreamingDelegate methods, in order to adapt xapp sdk a streaming player needs to
 * implement XAStreamingDelegate methods.
 *
 *  @since 3.12.0
 */
@protocol XAStreamingDelegate <NSObject>

///---------------------------------------------------------------------------------------
/// @name Protocol Methods
///---------------------------------------------------------------------------------------

/**
 * Informs the delegate to mute it's audio stream in order to listen for a user's response without
 * pausing or stopping the stream.
 *
 *  @since 3.12.0
 */
- (void)mute;

/**
 * Informs the delegate that the monitor has finished listening for the user and now the delegate
 * can unmute it's audio stream
 *
 *  @since 3.12.0
 */
- (void)unmute;

/**
 * Informs the delegate that the monitor has exceeded it's designated time slot, either because the
 * user responded to the ad or a timeout occurred during the user interaction. This requires the
 * delegate to stop it's stream until the resume function is called.
 *
 *  @since 3.12.0
 */
- (void)stop;

/**
 * The XAStreamingMonitor doesn't call resume unless it previously called the stop method. The
 * resume function informs the delegate that the monitor has completed a user interaction and the
 * delegate needs to resume the stream.
 *
 *  @since 3.12.0
 */
- (void)resume;

@optional
///---------------------------------------------------------------------------------------
/// @name Session Management
///---------------------------------------------------------------------------------------

/**
 *  Alerted when XAPP  has successfully started
 *
 *  @since 3.12.0
 */
- (void)onXappStarted;

/**
 *  Informs the delegate XAPP has failed to start and supplies the error that occured
 *
 *
 *  @since 3.12.0
 *  @param error The error that occured during startup
 */
- (void)onFailedXappStart:(NSError *)error;

@end
