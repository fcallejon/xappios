//
//  XAStreamingMonitor.h
//
//  Created by the developers at XAPPmedia in beautiful Washington, DC
//  Copyright (c) 2015 XAPPmedia. All rights reserved.
//

#import "XAStreamingAd.h"
#import "XAStreamingDelegate.h"
#import "XAPP.h"
#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString *const XAStreamingTerminate;

/** Block that passes XappLoaded callback to streaming monitor. */
typedef void (^XappLoaded)(NSError *error, BOOL success);
/** Block that passes XappStartedPlayback callback to streaming monitor. */
typedef void (^XappStartedPlayback)(NSError *error, BOOL success);
/** Block that passes XappCompleted callback to streaming monitor. */
typedef void (^XappCompleted)(NSError *error, BOOL success, XAResult *result);

/**
 *  The streaming monitor provides functionality that makes audio that is injected into live streams
 * interactive.
 *
 *  @since 3.12.0
 */
@interface XAStreamingMonitor : NSObject

/**
 *  Start the facade/plugin
 *
 *  @since 3.12.0
 *  @param theAPIKey Developer specific API key
 *  @param theApplicationKey Application key associated with application the plugin will be embedded
 * in
 *  @param theDelegate XAPPDelegate @see XAPPDelegate for receiving call backs from the plugin
 *  @return singelton instance
 */
+ (void)initMonitorWithAPIKey:(NSString *)theAPIKey
           withApplicationKey:(NSString *)theApplicationKey
              withAdsDelegate:(NSObject<XAStreamingDelegate> *)theDelegate;

///---------------------------------------------------------------------------------------
/// @name Callback Management
///---------------------------------------------------------------------------------------

/**
 *  When host player detects "Xapp" string in metadata it needs to parse that meta data and call
 * onXappMark block with correct paramaters to play a xapp. If host player designed to receive one
 * metadata per audio, please use this method to play a xapp.
 *
 *  @since 3.12.0
 *  @param xapp          a XAStreamingAd object with Xapp name and start time of the Xapp.
 *  @param loadBlock     loadBlock that will return when ad is loaded
 *  @param startBlock    startBlock that will return when ad starts to play
 *  @param completeBlock completeBlock that will return when ad is completed
 */
+ (void)onXappMark:(XAStreamingAd *)xapp
          loadXapp:(XappLoaded)loadBlock
   startedPlayback:(XappStartedPlayback)startBlock
      xappComplete:(XappCompleted)completeBlock;

/**
 *  Host player has the ability to call loadXapp method first and then to call the playXapp method
 * at will. This way host player has the option of when to play the loaded xapp. If host player
 * designed to receive multiple metadata per audio, please use this method to play a xapp.


 *  For example a start speaking cue is at the 23rd second of a xapp ad, host player gets some
 * metadata to load the xapp at 15th second of the audio file, then host player can call loadXapp
 * method then when host player receives “play” metadata or when host player reaches 23rd second of
 * the audio, host player can call playXapp method which will play loaded xapp at 15h second.
 *
 *  @since 3.12.0
 *  @param xapp `XAStreamingAd` object with Xapp name and start time of the Xapp.
 *  @param loadBlock    `XappLoaded` block that will return when ad is loaded
 */
+ (void)loadXapp:(XAStreamingAd *)xapp xappLoadCallBack:(XappLoaded)loadBlock;

/**
 *  Please read @see loadXapp:(XAStreamingAd *)xapp xappLoadCallBack:(XappLoaded)loadBlock
 *
 *  @since 3.12.0
 *  @param startBlock    startBlock that will return when ad starts to play
 *  @param completeBlock completeBlock that will return when ad is completed
 */
+ (void)playXapp:(XappStartedPlayback)startBlock xappComplete:(XappCompleted)completeBlock;

///---------------------------------------------------------------------------------------
/// @name Audio Playback Management
///---------------------------------------------------------------------------------------

/**
 *  Unregister timers. If called before listening cue, voice recognition doesn't start.
 *
 *  @since 3.12.0
 */
+ (void)playerStopped;

/**
 *  If ad requested , pauses timers. If host player is paused for any reason, this function needs to
 * be called so Xapp will fire on time.
 *
 *  @since 3.12.0
 */
+ (void)playerPaused;

/**
 *  If timers are paused, resumes timers. If host player decided resuming after pausing, this
 * function needs to be called so Xapp will fire on time.
 *
 *  @since 3.12.0
 */
+ (void)playerResumed;

+ (void)playerCancelled;

///---------------------------------------------------------------------------------------
/// @name Playback Controls
///---------------------------------------------------------------------------------------

/**
 *  Returns if the plugin has an ad presented.  The ad could not be presented but still playing
 * audio (adPlaying) or have the audio paused (adRunning).
 *
 *  @since 3.12.0
 *  @see adRunning
 *  @see adPlaying
 *  @return TRUE if an ad is presented
 */
+ (BOOL)adPresented;

/**
 *  Returns if the plugin is currently playing the advertisement
 *
 *
 *  @since 3.12.0
 *  @see adRunning
 *  @see adPresented
 *  @return TRUE if an ad is playing
 */
+ (BOOL)adPlaying;

/**
 *  Returns if the SDK is in the process of playing an advertisement.  The ad could be not
 * presented, and not playing audio but still in  a playback mode.
 *
 *
 *  @since 3.12.0
 *  @see adPlaying
 *  @see adPresented
 *  @return TRUE if the SDK is currently in ad playback mode
 */
+ (BOOL)adRunning;

@end
