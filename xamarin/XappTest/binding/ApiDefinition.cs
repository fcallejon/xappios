﻿using UIKit;
using Foundation;

namespace binding
{
	[BaseType(typeof(NSObject))]
	interface XappWrapper
	{
		[Export("start:withAppKey:fromViewController:")]
		void Start(string apiKey, string appKey, UIViewController controller);

		[Export("getNewAd")]
		void GetNewAd();

		[Export("playInterstitial:")]
		void PlayInterstitial(UIViewController controller);

		[Export("isReady")]
		bool IsReady { get; }

		[Export("isAdReady")]
		bool IsAdReady { get; }
	}
}
