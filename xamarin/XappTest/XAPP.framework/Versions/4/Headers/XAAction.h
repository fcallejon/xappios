//
//  XAAction.h
//  XappAds
//
//  Created by the developers at XAPPmedia in beautiful Washington, DC
//  Copyright (c) 2015 XAPPmedia. All rights reserved.
//
//  Your use of the XAPP SDK is governed by the XAPP SDK Terms of Service and License Agreement
//  http://xappmedia.com/xapp-sdk/sdk-terms/
//

#import <Foundation/Foundation.h>

@class XALinear;
@protocol XAPlaying;

/**
 *  The `XAActionType` categorizes the type of action and outlines what will occur.
 */
typedef NS_ENUM(NSInteger, XAActionType) {
    /** Action that will open a link to the iTunes Store */
    XAActionTypeStore = 0,
    /** Action that will make a phone call */
    XAActionTypeCall,
    /** Action that will return data (set when building the ad) on the result object if recognized
       */
    XAActionTypeCustom,
    /** Action that will send the user an email */
    XAActionTypeEmail,
    /** Action that will open a web page */
    XAActionTypeWebPage,
    /** Action that will play audio */
    XAActionTypeAudio,
    /** Action that will request permission */
    XAActionTypeRequestPermission
};

FOUNDATION_EXPORT NSString *_Nonnull const XAActionTypeStringAudio;
FOUNDATION_EXPORT NSString *_Nonnull const XAActionTypeStringCall;
FOUNDATION_EXPORT NSString *_Nonnull const XAActionTypeStringCustom;
FOUNDATION_EXPORT NSString *_Nonnull const XAActionTypeStringEmail;
FOUNDATION_EXPORT NSString *_Nonnull const XAActionTypeStringStore;
FOUNDATION_EXPORT NSString *_Nonnull const XAActionTypeStringWebPage;
FOUNDATION_EXPORT NSString *_Nonnull const XAActionTypeStringRequestPermission;

/** An action describes one of the possible options for recognition and workflow to be complete if
the action is recognized.  The potential actions are represented by the XAActionType enum.
 */
@interface XAAction : NSObject <NSCoding, NSCopying>

///---------------------------------------------------------------------------------------
/// @name Initialize an XAAction Object
///---------------------------------------------------------------------------------------

/**
 *  Create a new action with a dictionary of values
 *
 *  @param values Dictionary of values that are used to build the action.
 *
 *  @return A new action
 */
+ (nullable XAAction *)actionWith:(nonnull NSDictionary *)values;

+ (nonnull XAAction *)actionWithType:(XAActionType)type
                              phrase:(nonnull NSString *)phrase
                                 url:(nullable NSURL *)url
             foregroundTrailingAudio:(nullable id<XAPlaying>)foregroundTrailingAudio
             backgroundTrailingAudio:(nullable id<XAPlaying>)backgroundTrailingAudio;

+ (nonnull XAAction *)clickThroughActionWithUrl:(nonnull NSURL *)url;

+ (nonnull XAAction *)expansionActionWithPhrase:(nonnull NSString *)phrase
                                       audioUrl:(nonnull NSString *)audioUrl;

+ (nonnull XAAction *)customActionWithPhrase:(nonnull NSString *)phrase
                                    metadata:(nonnull NSString *)metadata;

///---------------------------------------------------------------------------------------
/// @name Universal Action Data
///---------------------------------------------------------------------------------------

/**
 *  XAActionType of the action.
 *
 *  @see XAActionType
 */
@property (nonatomic, readonly) enum XAActionType actionType;

/**
 *  The phrase for the action to be recognized.
 */
@property (nonnull, nonatomic, retain, readonly) NSString *phrase;

/**
 *  The trailing audio for the current state, either background or foreground.
 */
@property (nullable, nonatomic, retain, readonly) NSObject<XAPlaying> *trailingAudio;

/**
 *  Audio that is played, after the action is recognized, when the action is executed in the
 * foreground
 */
@property (nullable, nonatomic, retain, readonly) NSObject<XAPlaying> *trailingAudioForeground;

/**
 *  Audio that is played, after the action is recognized, when the action is executed in the
 * background.
 */
@property (nullable, nonatomic, retain, readonly) NSObject<XAPlaying> *trailingAudioBackground;

/**
 *  Linear creative assocaited with the action
 */
@property (nullable, nonatomic, retain, readonly) XALinear *linear;

/**
 *  The URL for the action.
 */
@property (nullable, nonatomic, retain, readonly) NSURL *url;

/**
 *  URL that a HTTP GET is performed when the action is performed, used for tracking the action.
 */
@property (nonnull, nonatomic, retain, readonly) NSArray *trackingEvents;

/**
 *  Metadata associated with the action, used for an XAActionTypeCustom (see XAActionType) action
 */
@property (nullable, nonatomic, retain, readonly) NSString *data;

///---------------------------------------------------------------------------------------
/// @name Helper Methods
///---------------------------------------------------------------------------------------

/**
 *  Returns the actionType of the action as a human readable string (instead of an integer).
 *
 *  @return A human readable string for the action type.
 */
- (nullable NSString *)actionTypeAsString;

/**
 *  Convert a string to an XAActionType.
 *
 *  Used internal to XAPPKit when converting the JSON payload to executable actions.
 *
 *  @param actionType A string representation of the XAActionType, one of the `XAActionTypeString`
 * constants
 *  @return The XAActionType that matches the provided string, if no match `XAActionTypeUnspecified`
 * is returned
 */
+ (enum XAActionType)stringAsActionType:(nonnull NSString *)actionType;

/**
 *  Convert the XAActionType to a human readable string, one of the `XAActionTypeString` constants
 *
 *  @param actionType Action type to translate to a string
 *  @return Returns a string representation of the provided XAActionType
 */
+ (nullable NSString *)actionTypeAsString:(enum XAActionType)actionType;

@end
