//
//  XAStreamingAd.h
//
//  Created by the developers at XAPPmedia in beautiful Washington, DC
//  Copyright (c) 2015 XAPPmedia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XASettings.h"

/**
 * XAStreamingAd is used with the XAStreamingMonitor to request the necessary metadata that is
 * associated with the ad that is already inejected in the live stream.
 *
 *  @since 3.12.0
 */
@interface XAStreamingAd : NSObject

/**
 *  Create a streaming ad for the provided tag
 *
 *  @since 3.12.0
 *  @param tag Tag for the ad that will play in the stream
 *  @return New instance for the streaming ad
 */
- (instancetype)initWithAdTag:(NSString *)tag;

/**
 *  The settings for the streaming ad.
 */
@property (nonatomic, readonly) XASettings *settings;

/**
 *  The tag for the ad that is injected in the live stream
 *
 *  @since 3.12.0
 */
@property (nonatomic, strong, readonly) NSString *adTag;

@property (nonatomic, strong) NSString *adImageUrl;

@property double adLength;

@property double adStartMark;

@end