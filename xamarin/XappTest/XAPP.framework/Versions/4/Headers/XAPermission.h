//
//  XAPermission.h
//  XAPP
//
//  Created by the developers at XAPPmedia in beautiful Washington, DC
//  Copyright (c) 2015 XAPPmedia. All rights reserved.
//
//  Your use of the XAPP SDK is governed by the XAPP SDK Terms of Service and License Agreement
//  http://xappmedia.com/xapp-sdk/sdk-terms/
//

#import <Foundation/Foundation.h>
@import UIKit;

FOUNDATION_EXPORT NSString *_Nonnull const XAPermissionTypeRecord;

/**
 *  Enumeration which describes the status of the permission request.
 */
typedef NS_ENUM(NSInteger, XAPermissionStatus) {
    /**
     *  The system permission has been accepted.
     */
    XAPermissionStatusAccepted,
    /**
     *  Displaying the system permission was deferred.
     */
    XAPermissionStatusDeferred,
    /**
     *  The system permission was denied.
     */
    XAPermissionStatusDenied,
    /**
     *  The requirements to present the permission workflow were not fulfilled.
     */
    XAPermissionStatusUnfulfilled
};

/**  The permission API is a set of methods that allow you request permissions from your user,
specifically the record permission.

The primary method is requestRecordPermission:, which will request a permission strategy from the
server and present it based on the strategie's requirements.

    [XAPermission requestRecordPermission:^(BOOL granted) {
        NSLog(@"%@", granted ? @"Permission Granted" : @"Permission Not Granted");
    }];

If this method is called before calling [XAPP
startWithAPIKey:withApplicationKey:withAdsDelegate:withUserData:], you will first need  to configure
the API with useAPIKey:applicationKey:

    [XAPermission useAPIKey:apiKey applicationKey:applicationKey];

For more information on permissions see

 - [Record Permission](http://developer.xappmedia.com/guides/record-permission/)
 - [Permissions API](http://developer.xappmedia.com/guides/permissions-api/)

 */
@interface XAPermission : NSObject

///---------------------------------------------------------------------------------------
/// @name Request Record Permission
///---------------------------------------------------------------------------------------

/**
 *  Request record permission, similar to [AVAudioSession's
 * requestRecordPermission](https://developer.apple.com/library/ios/documentation/AVFoundation/Reference/AVAudioSession_ClassReference/index.html#//apple_ref/occ/instm/AVAudioSession/requestRecordPermission:).
 *
 *  @since 3.13.0
 *  @param response Block that is called when the user either accepts defers, or denies record
 * permission.
 */
+ (void)requestRecordPermission:(void (^_Nonnull)(BOOL granted))response;

/**
 *  Request record permission.  The same as requestRecordPermission: however a specific
 * UIViewController is passed.
 *
 *  This method starts a permissioning workflow that will enage directly with the user, explaining
 * to
 *  them the need for record permission and present them a prepermission dialog.
 *
 *  @since 3.13.0
 *  @param viewController The view controller the workflow will be displayed over
 *  @param response       A callback for when the workflow is complete which returns `YES` if the
 * permission was accepted and `NO` if it was not granted.
 */
+ (void)fromViewController:(nonnull UIViewController *)viewController
   requestRecordPermission:(void (^_Nonnull)(BOOL granted))response;

///---------------------------------------------------------------------------------------
/// @name Configuration
///---------------------------------------------------------------------------------------

/**
 *  Set the application credentials required for requesting permission strategies.
 *
 *  Use of this method is required if you do not provide the credentials in your app's plist.
 *
 *  @since 3.13.0
 *  @param apiKey        API key string specific to your publisher
 *  @param aplicationkey Application key string specific to the current application
 */
+ (void)useAPIKey:(nonnull NSString *)apiKey applicationKey:(nonnull NSString *)aplicationkey;

///---------------------------------------------------------------------------------------
/// @name Testing and Debugging
///---------------------------------------------------------------------------------------

/**
 *  Present the permission strategy.  This method does not go on to request the actual system
 * permission and is used primarily for testing.  It is the underlying implementation of
 * requestRecordPermission:.
 *
 *  This will retrieve a permission strategy if one isn't available locally or attempt to present
 * one if one is available.
 *
 *  @since 3.13.0
 *  @param viewController The view controller the strategy workflow will be presented from
 *  @param permissionType The type of permission to present.
 *  @param response       A callback providing a status when the workflow is complete
 */
+ (void)fromViewController:(nonnull UIViewController *)viewController
             forPermission:(nonnull NSString *)permissionType
           presentStrategy:
               (void (^_Nonnull)(XAPermissionStatus status, NSError *_Nullable error))response;

/**
 *  Convert the XAPermissionStatus to a human readable string
 *
 *  @since 3.13.0
 *  @see    fromViewController:forPermission:presentStrategy:
 *  @param  status Permission status
 *  @return Corresponding string value for the status
 */
+ (nullable NSString *)permissionStatusToString:(XAPermissionStatus)status;

/**
 *  Clears the cached strategy.
 *
 *  When requestRecordPermisison: is called, it checks to see if it has a permission strategy
 * already cached.
 *
 *  @since 3.13.0
 *  @return Returns `YES` if the strategy was successfully cleared out, `NO` if there was no
 * strategy to clear or
 */
+ (BOOL)clearCachedStrategy;

/**
 *  Clears the cached list of excluded strategies.
 *
 *  Excluded strategies are strategies that were deferred by the user and should no longer be
 * presented to the user.
 *
 *  @since 3.13.0
 *  @return Returns `YES` if the strategy was successfully cleared, `NO` if there was no excluded
 * list to clear or it was uncessful.
 */
+ (BOOL)clearExcludedStrategies;

/**
 *  Use the development server.  You should not call this.
 */
+ (void)useDevServer;

@end
