//
//  XAMicAnimationProtocol.h
//  XAPP
//
//  Created by Umut Genlik on 5/12/16.
//  Copyright © 2016 XAPPmedia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 *  The `XAMicAnimationProtocol` provides a protocl for developers to adopt in their custom
 * microphone animation classes.
 */
@protocol XAMicAnimationProtocol <NSObject>

/*
 * Tells the waveform/animation to redraw itself using the given level 0 to 1 decibel levels
 */
- (void)startAnimatingWithLevel:(CGFloat)level;

/**
 *  This method will set the right frame to layer
 *
 *  @param frame current frame.
 */
- (void)updateFrame:(CGRect)frame;

/**
 *  It will hide the layer.
 *
 *  @param hidden YES is hidden
 */
- (void)setHidden:(BOOL)hidden;

/**
 *  Remove Layer, this method remove the animation first, and then remove the layer from superview
 */
- (void)stopAndRemoveLayer;

/**
 * You DON'T need to actaully add set or get any methods for the view, this is added so xapp sdk
 * wont throw a warning because if you are using a view controller xapp sdk needs
 * to access to view to add it ass a child view controller.
 */
@optional
@property (nonatomic) UIView *view;

@end
