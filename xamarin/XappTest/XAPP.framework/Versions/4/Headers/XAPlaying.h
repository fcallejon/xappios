//
//  XAPlayable.h
//  XAPP
//
//  Created by Michael Myers on 4/20/16.
//  Copyright © 2016 XAPPmedia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <XAPP/XAPlayingDelegate.h>

/**
 *   The state of the playable object
 */
typedef NS_ENUM(NSInteger, XAPlayingState) {
    /** State Paused */
    XAPlayingStatePause,
    /** State Playing */
    XAPlayingStatePlay,
    /** State Reseted */
    XAPlayingStateReset,
    /** State Stopped*/
    XAPlayingStateStop
};

/**
 *  Protocol for objects that can be played.
 */
@protocol XAPlaying <NSObject>

/**
 *  The delegate for the playable object, receives updates on playback status through the delegate
 */
@property (nullable, readwrite) NSObject<XAPlayingDelegate> *delegate;

/**
 *  The duration of the playable content.
 */
@property (readonly) NSTimeInterval duration;

/**
 *  The current time into the playable object
 */
@property (nonatomic, readonly) NSTimeInterval currentTime;

/**
 *  If the playable object is currently playing
 */
@property (nonatomic, readonly, getter=isPlaying) BOOL playing;

/**
 *  If the playable object can be skipped
 */
@property (nonatomic, readonly, getter=isSkippable) BOOL skippable;

/**
 * Play
 */
- (void)play;

/**
 * Pause
 */
- (void)pause;

/**
 * Stop
 */
- (void)stop;

@end
