//
//  XACompanionViewDelegate.h
//  XAPP
//
//  Created by Michael Myers on 6/6/16.
//  Copyright © 2016 XAPPmedia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class XAAction;

@protocol XACompanionDelegate <NSObject>

@required
- (void)companionRequestedAction:(XAAction *)action;

- (void)companionRequestedSkip;

@optional

- (void)companionTouchedAtPoint:(CGPoint)point;

@end
