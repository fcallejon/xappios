//
//  XAPP.h
//  XAPP
//
//  Created by the developers at XAPPmedia in beautiful Washington, DC
//  Copyright (c) 2015 XAPPmedia. All rights reserved.
//
//  Your use of the XAPP SDK is governed by the XAPP SDK Terms of Service and License Agreement
//  http://xappmedia.com/xapp-sdk/sdk-terms/
//

#import "XARequest.h"
#import "XAResult.h"
#import "XAUserData.h"
#import "XAAdvertisement.h"
#import <Foundation/Foundation.h>
#import "XAMicAnimationProtocol.h"

FOUNDATION_EXPORT NSString *const XAPPVersionString;
FOUNDATION_EXPORT NSString *const XAPPBuildString;

/** Possible logging levels that output to the development console.  The log level is set with
 * [XAPP setLogLevel:].
 *  @see [XAPP setLogLevel:]
 */
typedef NS_ENUM(NSUInteger, XALogLevel) {
    /** Debug log level, the most verbose logging, prints every subsequent level */
    XALogLevelDebug = 0,
    /** Info log level, prints out only status changes and important events, as well as warnings and
     errors*/
    XALogLevelInfo,
    /** Warn log level, prints out warnings and errors */
    XALogLevelWarn,
    /** Error log level, only prints out errors */
    XALogLevelError
};

/** XAPP SDK, from [XAPPmedia](https://xappmedia.com/ "XAPPmedia"), is a plugin for iOS applications
 which serves interactive audio advertisements.
 */
@interface XAPP : NSObject

///---------------------------------------------------------------------------------------
/// @name Initialization and Configuration
///---------------------------------------------------------------------------------------

/**
 *  Start the plugin
 *
 *  @param theAPIKey Developer specific API key
 *  @param theApplicationKey Application key associated with application the plugin will be embedded
 */
+ (void)startSessionWithAPIKey:(NSString *)theAPIKey
            withApplicationKey:(NSString *)theApplicationKey;

/**
 *  Start the plugin
 */
+ (void)startSession;

/**
 *  Returns if the plugin has started
 *
 *  @return TRUE if the plugin is running, FALSE if not
 */
+ (BOOL)pluginStarted;

/**
 *  User microphone permission
 *
 * @return If the user has granted permission to use the microphone
 */
+ (BOOL)microphonePermission;

///---------------------------------------------------------------------------------------
/// @name Advertisement Requests
///---------------------------------------------------------------------------------------

/**
 *  Informs the block the request was successful and supplies the loaded advertisement.
 *
 *  @param request The XAAdRequest
 *  @param success block for The loaded advertisement
 *  @param failure block for The failed advertisement
 */
+ (void)requestAd:(XARequest *)request
          success:(void (^)(XAAdvertisement *spot))success
          failure:(void (^)(NSError *error))failure;

///---------------------------------------------------------------------------------------
/// @name Advertisement Playback, In-Tuner Tile or Full Screen Interstitial
///---------------------------------------------------------------------------------------

/**
 *  Play the advertisement with the companion ad within a view
 *
 *  @param advertisement Advertisement to be played
 *  @param view          The view to display the companion ad within
 *  @param start         Block called when the audio starts
 *  @param success       Block called when the ad completes successfully
 *  @param failure       Block called when the ad fails to complete
 */
+ (void)playAdAsInTunerTile:(XAAdvertisement *)advertisement
         withContainingView:(UIView *)view
            startedPlayback:(void (^)(XAAdvertisement *spot))start
                    success:(void (^)(XAAdvertisement *spot, XAResult *result))success
                    failure:(void (^)(XAAdvertisement *spot, NSError *error))failure;

/**
 *  Play the advertisement with the companion ad displayed full-screen
 *
 *  @param advertisement  The advertisement that will be played
 *  @param viewController The View controller to present the interstitial companion from
 *  @param start          Block called when the audio starts
 *  @param success        Block called when the ad completes successfully
 *  @param failure        Block called when the ad fails to complete
 */
+ (void)playAdAsInterstitial:(XAAdvertisement *)advertisement
      withRootViewController:(UIViewController *)viewController
             startedPlayback:(void (^)(XAAdvertisement *spot))start
                     success:(void (^)(XAAdvertisement *spot, XAResult *result))success
                     failure:(void (^)(XAAdvertisement *spot, NSError *error))failure;

/**
 *  Play the advertisement without a companion ad
 *
 *  @param advertisement  The advertisement that will be played
 *  @param start          Block called when the audio starts
 *  @param success        Block called when the ad completes successfully
 *  @param failure        Block called when the ad fails to complete
 */
+ (void)playAdAsAudioOnly:(XAAdvertisement *)advertisement
          startedPlayback:(void (^)(XAAdvertisement *spot))start
                  success:(void (^)(XAAdvertisement *spot, XAResult *result))success
                  failure:(void (^)(XAAdvertisement *spot, NSError *error))failure;

/**
 *  Start listening for the advertisement, skips the audio.
 *
 *  If an action is requested by the user, the action will be performed.
 *
 *  @param advertisement Advertisement with available voice actions.
 *  @param result        A block that is called when the action is complete.
 */
+ (void)startListeningForAdvertisement:(XAAdvertisement *)advertisement
                            withResult:(void (^)(XAResult *result, NSError *error))result;

///---------------------------------------------------------------------------------------
/// @name Playback Controls
///---------------------------------------------------------------------------------------

/**
 *  Pause the playback of the advertisement
 */
+ (void)pauseAd;

/**
 *  Resume the playback of the advertisement
 */
+ (void)resumeAd;

/**
 *  Cancel the advertisement
 *
 *  @param advertisement The advertisement to be cancelled
 */
+ (void)cancelAd:(XAAdvertisement *)advertisement;

/**
 *  Returns if the plugin has an ad presented.  The ad could not be presented but still playing
 * audio (adPlaying) or have the audio paused (adRunning).
 *
 *  @see adRunning
 *  @see adPlaying
 *  @return TRUE if an ad is presented
 */
+ (BOOL)adPresented;

/**
 *  Returns if the plugin is currently playing the advertisement
 *
 *  @see adRunning
 *  @see adPresented
 *  @return TRUE if an ad is playing
 */
+ (BOOL)adPlaying;

/**
 *  Returns if the SDK is in the process of playing an advertisement.  The ad could be not
 * presented, and not playing audio but still in  a playback mode.
 *
 *  @see adPlaying
 *  @see adPresented
 *  @since 3.8.0
 *  @return TRUE if the SDK is currently in ad playback mode
 */
+ (BOOL)adRunning;

///---------------------------------------------------------------------------------------
/// @name Advertisement Availability
///---------------------------------------------------------------------------------------

/**
 *  Returns if an advertisement is available for playback
 *
 *  @return TRUE if an advertisment if available, FALSE if no advertisement is available for
 * playback
 */
+ (BOOL)adAvailable;

/**
 *  Retrieve the next ad.
 *
 *  @return The next available advertisement based on a first requested, first out basis.
 */
+ (XAAdvertisement *)nextAdvertisement;

/**
 *  Retrieve the current advertisement queue
 *
 *  The plugin stores a queue of the advertisements that were requested with requestAd:
 *
 *  @return An array of XAAdvertisement objects
 */
+ (NSArray *)availableAdvertisements;

///---------------------------------------------------------------------------------------
/// @name Configure XAPP
///---------------------------------------------------------------------------------------

/**
 *  Set the XALogLevel during development, default is XALogLevelWarn.  This is typically only used
 * to produce logs for XAPPmedia support.
 *
 *  @warning Use only for debugging purposes, make sure to return to the default before submitting
 * to the app store
 *  @param logLevel XALogLevel
 */
+ (void)setLogLevel:(XALogLevel)logLevel;

/**
 *  Configure the ad request timeout interval
 *
 *  @param timeout Ad request timeout in seconds
 */
// TODO: Set this on the settings object
+ (void)setRequestAdTimout:(NSTimeInterval)timeout;

/**
 *  Use the development server.
 *
 *  Your credentials will will not work if you call this.
 *
 *  @since 4.0.0
 */
+ (void)useDevServer;

@end
