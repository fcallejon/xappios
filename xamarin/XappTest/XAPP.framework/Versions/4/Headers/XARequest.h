//
//  XAAdRequest.h
//  XappAds
//
//  Created by the developers at XAPPmedia in beautiful Washington, DC
//  Copyright (c) 2015 XAPPmedia. All rights reserved.
//
//  Your use of the XAPP SDK is governed by the XAPP SDK Terms of Service and License Agreement
//  http://xappmedia.com/xapp-sdk/sdk-terms/
//

#import <Foundation/Foundation.h>
@class XAUserData;

/**
 *  Defines the standard advertisement lengths, used on an ad request.
 *
 *  @warning This filtering parameter is currently not being used
 */
typedef NS_ENUM(NSUInteger, XADuration) {
    /** The length is unspecified, this is the default value and does not need to be set */
    XADurationUnspecified = 0,
    /** 10 seconds */
    XADurationTenSecond = 10,
    /** 15 seconds */
    XADurationFifteenSecond = 15,
    /** 20 seconds */
    XADurationTwentySecond = 20,
    /** 30 seconds */
    XADurationThirtySecond = 30,
    /** 60 seconds */
    XADurationSixtySecond = 60
};

FOUNDATION_EXPORT NSString *const XAAdRequestParameterAge;
FOUNDATION_EXPORT NSString *const XAAdRequestParameterAudioRoute;
FOUNDATION_EXPORT NSString *const XAAdRequestParameterCalendarPermission;
FOUNDATION_EXPORT NSString *const XAAdRequestParameterCallAbility;
FOUNDATION_EXPORT NSString *const XAAdRequestParameterEmailAvailable;
FOUNDATION_EXPORT NSString *const XAAdRequestParameterEnabled;
FOUNDATION_EXPORT NSString *const XAAdRequestParameterGender;
FOUNDATION_EXPORT NSString *const XAAdRequestParameterGenderFemale;
FOUNDATION_EXPORT NSString *const XAAdRequestParameterGenderMale;
FOUNDATION_EXPORT NSString *const XAAdRequestParameterMicrophonePermission;

/**  A `XAAdRequest` object is used to request a new advertisement from the XAPPmedia Creative
 Server.  The type of ad can be specified as well as the type of display for an ad request.  If
 available, contextual data can be attached to the ad request to help provide more relavent ads to
 the application user.

 `XAAdRequest` objects are passed to ```[XAPP requestAd:]```, which requests the ad and then
 notifies
 the [XAPPDelegate adRequest:didFinishLoadWithView:] on a succesful request and [XAPPDelegate
 adRequest:didFailLoadWithError:] on a failed request.

 ## Basic Usage

 When relying on a third-party ad server, use the adRequestWithAdTag: method to request a specific
 ad.

    XAAdRequest *request = [XARequest requestWithTag:<#Tag#>];

 To rely on simple server side ad mediation, simply pass an initialized ad request.

    XARequest *request = [[XARequest alloc] init];

 DFP can also be used for server side ad mediation, create a request with the desired network ID, ad
 unit ID, and optional parameters.

    XARequest request = [XARequest adRequestForDfpWithNetworkId:<#Network ID#>
                                                      andAdUnit:<#Ad Unit ID#>
                                                 withParameters:<#Parameters#>];

 */
@interface XARequest : NSObject <NSCopying>

///---------------------------------------------------------------------------------------
/// @name Initializing an XAAdRequest Object
///---------------------------------------------------------------------------------------

/**
 *  Request an ad by tag
 *
 *  When relying on a third-party ad server for mediation, use this method to request an ad by tag
 * from the XAPPmedia Creative Server.
 *
 *  @param tag The tag, typically in form *_Account Name_/_Ad Name_*
 *  @return The request with provided tag
 *  @see tag
 *  @since Available in 3.6.0 and later
 */
+ (instancetype)requestWithTag:(NSString *)tag;

/**
 *  Request ad through DFP.
 *
 *  @since Available in 3.6.0 or later
 *  @see adRequestForDfpWithNetworkId:andAdUnit:withParameters
 *  @return An instance of this class
 */
+ (instancetype)adRequestForDfp;

/**
 *  Request ad through DFP service
 *
 *  @since Available in 3.6.0 or later
 *  @param networkId DFP Network Identifier. If you want to use the default
 *                  value, set this to nil, in which case adUnit must also be nil
 *  @param adUnit Ad unit name. If you want to use default settings, set this
 *               to nil in which case networkId must be nil
 *  @param parameters DFP parameters. Can be nil
 *  @return An instance of this class
 */
+ (instancetype)adRequestForDfpWithNetworkId:(NSNumber *)networkId
                                   andAdUnit:(NSString *)adUnit
                              withParameters:(NSDictionary *)parameters;

///---------------------------------------------------------------------------------------
/// @name Request Information
///---------------------------------------------------------------------------------------

/**
 *  User data associated with the advertisement.
 *  Currently only used for phone calls where we call the user.
 */
@property (nonatomic, retain) XAUserData *userData;

/**
 *  Univerisal unique identifier for the request
 */
@property (nonatomic, retain, readonly) NSString *uuid;

/**
 *  The tag of the ad to be requested, in the form *_Account Name_/_Ad Name_*
 *
 *  @since Available in 3.6.0 and later
 */
@property (nonatomic, retain) NSString *tag;

/**
 *  The desired duration of the advertisement to be received
 *
 *  @warning Placeholder, this is currently not used
 */
@property (nonatomic, assign) XADuration duration;

/**
 *  Set `YES` if DFP is to be used for ad mediation
 *
 *  @since Available in 3.6.0 and later
 */
@property (nonatomic) BOOL dfpRequest;

/**
 *  Network ID for DFP ad mediation, dfpRequest must be `YES` if used
 *
 *  @since Available in 3.6.0 and later
 */
@property (nonatomic) NSNumber *networkId;

/**
 *  Ad Unit ID for DFP ad mediation, dfpRequest must be `YES` if used
 *
 *  @since Available in 3.6.0 and later
 */
@property (nonatomic, retain) NSString *adUnit;

/**
 *  Additional parameters used for targeting with DFP ad mediation
 *
 *  @since Available in 3.6.0 and later
 */
@property (nonatomic) NSDictionary *dfpParameters;

///---------------------------------------------------------------------------------------
/// @name Contextual Data
///---------------------------------------------------------------------------------------

/**
 *  The current genre the user is listening to.  This is used for targeting purposes.
 *
 *  @warning Placeholder, this is currently not used
 */
@property (nonatomic, retain) NSString *genre;

/**
 *  The current station format.  This is used for targeting purposes.
 *
 *  @warning Placeholder, this is currently not used
 */
@property (nonatomic, retain) NSString *stationFormat;

/**
 *  Add contextual keywords to the request for the current application context
 *
 *  @param keyword Contextual keyword to add to the request
 *  @warning Placeholder, this is currently not used
 */
- (void)addKeyword:(NSString *)keyword;

/**
 *  Get the keywords as a comma delimited string, used to easily pass to an ad server as meta-data.
 *
 *  @return A string of the keywords comma delimited
 *  @warning Placeholder, this is currently not used
 */
- (NSString *)keywordsAsCommaDelimitedString;

///---------------------------------------------------------------------------------------
/// @name Location Data
///---------------------------------------------------------------------------------------

/**
 *  The current latitude of the application user
 *
 *  @warning Placeholder, this is currently not used
 */
@property (nonatomic, assign, readonly) double latitude;

/**
 *  The current longitude of the application user
 *
 *  @warning Placeholder, this is currently not used
 */
@property (nonatomic, assign, readonly) double longitude;

/**
 *  Helper method to set the location as GPS coordinates of the application user
 *
 *  @param latitude  The current latitude of the device used for targeting purposes
 *  @param longitude The current longitude of the device used for targeting purposes
 *  @warning Placeholder, this is currently not used
 */
- (void)setLocationWithLatitude:(double)latitude longitude:(double)longitude;

/**
 *  Set the location with a description.
 *
 *  If location of the application user cannot be expressed in coordinates and the general location
 * is known, it can be set with a string description, such as "123 Main St. City, State" or more
 * commonly the zip code.
 *
 *  @param locationDescription The location description to be used for targeting by location
 *  @warning Placeholder, this is currently not used
 */
- (void)setLocationWithDescription:(NSString *)locationDescription;

///---------------------------------------------------------------------------------------
/// @name Development Methods
///---------------------------------------------------------------------------------------

/**
 *  Used to request the latest ad from the XAPPmedia Creative Server.
 *
 *  @warning *Warning* This is a test property and may not exist in future releases
 */
@property (nonatomic, assign) BOOL latestAd;

/**
 *  Application Key for the request, used only for bulk ad requests
 *
 *  @warning *Warning* This is a test property and may not exist in future releases
 */
@property (nonatomic, copy) NSString *appKey;

/**
 *  API Key for the request, used only for bulk ad requests
 *
 *  @warning *Warning* This is a test property and may not exist in future releases
 */
@property (nonatomic, copy) NSString *apiKey;

@end
