//
//  XAConfig.h
//  XappAds
//
//  Created by Michael Myers on 3/19/13.
//  Copyright (c) 2013 xappmedia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XAPP.h"
#import "XAMicAnimationProtocol.h"

/**
 *  XASettings provides settings for playback modification.
 */
@interface XASettings : NSObject <NSCoding>

/**
 *  Generate the settings from a dictionary
 *
 *  @param values Dictionary
 *
 *  @return New settings with values from the dictionary.
 */
+ (XASettings *)settingsWith:(NSDictionary *)values;

///---------------------------------------------------------------------------------------
/// @name Credentials
///---------------------------------------------------------------------------------------

@property (nonatomic) NSString *appKey;
@property (nonatomic) NSString *apiKey;

///---------------------------------------------------------------------------------------
/// @name Bluetooth Settings
///---------------------------------------------------------------------------------------
/** Boolean to disable all playback over AirPlay */
@property (nonatomic) BOOL airPlayPlayback;

///---------------------------------------------------------------------------------------
/// @name Display Settings
///---------------------------------------------------------------------------------------

@property (nonatomic, readonly) NSTimeInterval skipOffset;
@property (nonatomic, readonly) NSTimeInterval interstitialSkipOffset;

///---------------------------------------------------------------------------------------
/// @name Playback Settings
///---------------------------------------------------------------------------------------

/**
 *  Threshold to cancel playback if the audio progress stalls.
 */
@property (nonatomic) NSTimeInterval playbackStallThreshold;

///---------------------------------------------------------------------------------------
/// @name XVR Settings
///---------------------------------------------------------------------------------------

@property (nonatomic, copy) NSString *xvrCloudUrl;
/** Configuration parameters for speech cloud engine */
@property (nonatomic, strong) NSDictionary *speechCloudConfiguration;
/** Speech acceptance/reject threshold level */
@property double confidenceLevelThreshold;

///---------------------------------------------------------------------------------------
/// @name End Point Settings
///---------------------------------------------------------------------------------------

@property (nonatomic, retain, readonly) NSURL *server;
@property (nonatomic, readonly) NSTimeInterval adRequestTimeout;
@property (nonatomic, readonly) NSString *remoteLoggingUrl;

///---------------------------------------------------------------------------------------
/// @name General Settings
///---------------------------------------------------------------------------------------

@property (nonatomic) BOOL useDevServer; /** Boolean to switch over to the development servers */
@property (nonatomic) BOOL performCustomActionOnTouch; /** When a touch occurs, perform the custom action workflow */
@property (nonatomic) BOOL disableInAppBrowser; /** Disable the in-app browser in favor of opening the link in foreground or using the SFSafariViewController */

/**
 *  Force loading of all resources before the ad is ready for playback.
 */
@property (nonatomic) BOOL conservativeLoad;

///--------------------------------------
/// @name Midroll
///--------------------------------------

@property (nonatomic) BOOL midrollMode;
@property (nonatomic) NSUInteger fillerLength;

@end
