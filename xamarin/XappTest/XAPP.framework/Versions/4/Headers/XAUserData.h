//
//  XAUserData.h
//  XappAds
//
//  Created by the developers at XAPPmedia in beautiful Washington, DC
//  Copyright (c) 2015 XAPPmedia. All rights reserved.
//
//  Your use of the XAPP SDK is governed by the XAPP SDK Terms of Service and License Agreement
//  http://xappmedia.com/xapp-sdk/sdk-terms/
//

#import <Foundation/Foundation.h>

/**
 * User data is optional and can be used to better target an ad when targeting is available for an
 * ad.
 */
@interface XAUserData : NSObject <NSCopying>

///---------------------------------------------------------------------------------------
/// @name Location Data
///---------------------------------------------------------------------------------------

/**
 If location of the application user cannot be expressed in coordinates and the general location is
 known, it can be set with a string description, such as "101 Broad St, City, State" or "City,
 State"
 @param locationDescription The location description as a string
 */
- (void)setLocationWithDescription:(NSString *)locationDescription;

///---------------------------------------------------------------------------------------
/// @name Demographic Data
///---------------------------------------------------------------------------------------

/**
 *  The email of the current application user
 */
@property (nonatomic, retain) NSString *email;
/**
 *  The first name of the current application user
 */
@property (nonatomic, retain) NSString *firstName;
/**
 *  The last name of the current application user
 */
@property (nonatomic, retain) NSString *lastName;
/**
 *  The street address of the current application user
 */
@property (nonatomic, retain) NSString *streetAddress;
/**
 *  The city of the current application user
 */
@property (nonatomic, retain) NSString *city;
/**
 *  The state of the current application user
 */
@property (nonatomic, retain) NSString *state;
/**
 *  The zip code of the current application user
 */
@property (nonatomic, retain) NSString *zipCode;
/**
 *  The county of the current application user
 */
@property (nonatomic, retain) NSString *county;
/**
 *  The home phone of the current application user
 */
@property (nonatomic, retain) NSString *homePhone;
/**
 *  The mobile phone of the current application user
 */
@property (nonatomic, retain) NSString *mobilePhone;
/**
 *  The marital status of the current application user
 */
@property (nonatomic, retain) NSString *maritalStatus;
/**
 *  The gender of the current application user, either "male" or "female"
 */
@property (nonatomic, retain) NSString *gender;
/**
 *  The education level of the current application user
 */
@property (nonatomic, retain) NSString *education;
/**
 *  The number of children the current user has
 */
@property (nonatomic, retain) NSNumber *children;
/**
 *  The ethnicity of the application user
 */
@property (nonatomic, retain) NSString *ethnicity;
/**
 *  The political affiliation of the application user
 */
@property (nonatomic, retain) NSString *politics;
/**
 Holds developer added demographic data as a key value store
 */
@property (nonatomic, retain) NSMutableDictionary *demographicData;
/** Adds a new peice of demographic data by key.

 Used when demographic data exists that does not fit one of the class properites.  Add the data as a
 key value pair, for example "DOG" and "TRUE" for someone that has a dog.

 @param data The demographic data
 @param key The category for the data
 */
- (void)addDemographicData:(NSString *)data forKey:(NSString *)key;

/**
 *  The age of the listener
 */
@property (nonatomic, retain) NSNumber *age;

/**
 *  The birthday of the application user
 */
@property (nonatomic, retain) NSDate *birthday;

/**
 *  A helper method to set the birthday of the current applicaton user
 *
 *  @param month The month
 *  @param day   The day
 *  @param year  The year
 */
- (void)setBirthdayWithMonth:(NSInteger)month day:(NSInteger)day year:(NSInteger)year;

/**
 *  A helper function to set the birthday of the current application user based on their age
 *
 *  @param age Age of the listener
 */
- (void)setBirthdayWithAge:(NSInteger)age;

///---------------------------------------------------------------------------------------
/// @name Contextual Data
///---------------------------------------------------------------------------------------

/** Add contextual keywords to the request for the current application context
 @param keyword A contextual keyword string
 */
- (void)addKeyword:(NSString *)keyword;

/** Get the keywords as a comma delimited string
 @return Returns nil if keywords have not been set or the keywords as a comma delimited string
 */
- (NSString *)keywordsAsCommaDelimitedString;

@end
