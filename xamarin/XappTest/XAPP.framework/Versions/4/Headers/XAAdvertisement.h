//
//  XAAdvertisement.h
//  XappAds
//
//  Created by Michael Myers on 2/14/13.
//  Copyright (c) 2013 xappmedia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class XALinear, XAUserData, XASettings, XAAdComponents;
@protocol XACompanion, XAComplementaryCompanion, XALeaveBehindCompanion;

/**
 *  The `XAAdvertisement` is the main business object that contains all the necessary information
 *  for ad playback.
 */
@interface XAAdvertisement : NSObject

///---------------------------------------------------------------------------------------
/// @name Initialize an Advertisement
///---------------------------------------------------------------------------------------

/**
 *  Create an advertisement from a dictionary of values
 *
 *  @param dictionary A dictionary of values
 *
 *  @return A new advertisement
 */
+ (XAAdvertisement *)advertisementWith:(NSDictionary *)dictionary;

/**
 *  Create an advertisement with the supplied ad components.
 *
 *  @param components An ad components object.
 *
 *  @return A new advertisement
 */
+ (XAAdvertisement *)advertisementWithComponents:(XAAdComponents *)components;

/**
 *  Create an advertisement with the required components for DAAST.
 *
 *  @param title              The title for the advertisement.
 *  @param category           The category of the advertisement.
 *  @param impressionTracking An array or URLs that are used for tracking if an impression was
 * served.
 *  @param creatives          An array of creatives for the advertisement.
 *
 *  @return A new advertisement
 */
+ (XAAdvertisement *)advertisementWithTitle:(NSString *)title
                                   category:(NSString *)category
                         impressionTracking:(NSArray *)impressionTracking
                                   creative:(NSArray *)creatives;

/**
 *  Create an advertisement with the required components for DAAST as well as a settings object.
 *
 *  @param title              The title for the advertisement.
 *  @param category           The category of the advertisement.
 *  @param impressionTracking An array of URLs that are used for tracking if an impression was
 * served.
 *  @param creatives          An array of creatives for the advertisement.
 *  @param settings           A settings object for configuring playback.
 *
 *  @return A new advertisement
 */
+ (XAAdvertisement *)advertisementWithTitle:(NSString *)title
                                   category:(NSString *)category
                         impressionTracking:(NSArray *)impressionTracking
                                   creative:(NSArray *)creatives
                                   settings:(XASettings *)settings;

/**
 *  Settings for the advertisement, used to customize items such as stall thresholds, loading
 * timeouts, display preferences.
 */
@property (nonatomic, retain, readonly) XASettings *settings;

///---------------------------------------------------------------------------------------
/// @name Advertisement Data
///---------------------------------------------------------------------------------------

/**
 *  The request ID associated with the advertisement.
 *
 *  Used within XAPP to tie off to the particular request.
 */
@property (nonatomic, retain, readonly) NSString *requestKey;

/**
 *  The key associated with the session the advertisement was served within.
 */
@property (nonatomic, retain, readonly) NSString *sessionKey;

/**
 *  Now Playing Text is used for lock screen controls and bluetooth devices
 */
@property (nonatomic, retain, readonly) NSString *nowPlayingText;

/**
 *  User data associated with the advertisement.
 *  Currently only used for phone calls where we call the user.
 */
@property (nonatomic, retain) XAUserData *userData;

/**
 *  All the interactions for the ad from the companion views and the linear creatives.
 */
@property (nonatomic, retain, readonly) NSArray *adInteractions;

///---------------------------------------------------------------------------------------
/// @name DAAST Required
///---------------------------------------------------------------------------------------

/**
 *  The ID associated with the advertisement playback.
 *
 *  See [DAAST 3.1.2.1](http://www.iab.com/wp-content/uploads/2015/12/DAAST_1.1-3.pdf) Ad Attribures
 */
@property (nonatomic, retain, readonly) NSString *adID;

/**
 *  The name of the advertisement
 *
 *  See [DAAST 3.1.4.1 Required <Inline> Elements -
 * <AdTitle>](http://www.iab.com/wp-content/uploads/2015/12/DAAST_1.1-3.pdf)
 */
@property (nonatomic, retain, readonly) NSString *adTitle;

/**
 *  An array of URLs that are called when the ad has registered an impression.
 *
 *  See [DAAST 3.1.4.1 Required <Inline> Elements -
 * <Impression>](http://www.iab.com/wp-content/uploads/2015/12/DAAST_1.1-3.pdf)
 */
@property (nonatomic, retain, readonly) NSArray *impressionTracking;

/**
 *  The main linear creative for the ad.
 *
 *  See [DAAST 3.1.4.1 Required <Inline> Elements -
 * <Creatives>](http://www.iab.com/wp-content/uploads/2015/12/DAAST_1.1-3.pdf)
 */
@property (nonatomic, retain, readonly) XALinear *linearCreative;

/**
 *  An array of creative, either companion or linear creative.
 *
 *  See [DAAST 3.1.4.1 Required <Inline> Elements -
 * <Creatives>](http://www.iab.com/wp-content/uploads/2015/12/DAAST_1.1-3.pdf)
 */
@property (nonatomic, retain, readonly) NSArray *creatives;

/**
 *  The category for the ad.
 *
 *  See [DAAST 3.1.4.1 Required <Inline> Elements -
 * <Category>](http://www.iab.com/wp-content/uploads/2015/12/DAAST_1.1-3.pdf)
 */
@property (nonatomic, retain, readonly) NSString *category;

///---------------------------------------------------------------------------------------
/// @name DAAST Optional
///---------------------------------------------------------------------------------------

/**
 *  The server that returned the advertisement.
 *
 *  See [DAAST 3.1.4.2 Optional <Inline> Elements -
 * <AdSystem>](http://www.iab.com/wp-content/uploads/2015/12/DAAST_1.1-3.pdf)
 */
@property (nonatomic, retain, readonly) NSString *adSystem;

/**
 *  A description for the ad.
 *
 *  See [DAAST 3.1.4.2 Optional <Inline> Elements -
 * <Description>](http://www.iab.com/wp-content/uploads/2015/12/DAAST_1.1-3.pdf)
 */
@property (nonatomic, retain, readonly) NSString *adDescription;

/**
 *  The account for the advertisement
 *
 *  See [DAAST 3.1.4.2 Optional <Inline> Elements -
 * <Advertiser>](http://www.iab.com/wp-content/uploads/2015/12/DAAST_1.1-3.pdf)
 */
@property (nonatomic, retain, readonly) NSString *advertiser;

/**
 *  The companion for the advertisement that is displayed during playback.
 */
@property (nonatomic, retain, readonly) NSObject <XACompanion, XAComplementaryCompanion> *complementaryCompanion;

/**
 *  The companion for the advertisement that is left behind after the ad finishes.
 */
@property (nonatomic, retain, readonly) NSObject <XACompanion, XALeaveBehindCompanion> *leaveBehindCompanion;

///---------------------------------------------------------------------------------------
/// @name Advertisement Playback
///---------------------------------------------------------------------------------------

/**
 *  Check if the device has the necessary capabilities to play the ad properly.
 *
 *  @param error Reference to an error whereas if the ad cannot be played, the error will be
 * initialized with the appropriate error
 *  @return BOOL if the ad can be played on the current device
 */
- (BOOL)deviceCanPlay:(NSError **)error;

// Not available for use
- (instancetype)init NS_UNAVAILABLE;

@end
