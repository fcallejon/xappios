//
//  XAAdAudioDelegate.h
//  XAPP
//
//  Created by Umut Genlik on 4/12/16.
//  Copyright © 2016 XAPPmedia. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol XAPlaying;

typedef NS_ENUM(NSInteger, XAPlayingDurationThreshold) {
    XAPlayingDurationThresholdStart,
    XAPlayingDurationThresholdFirstQuartile,
    XAPlayingDurationThresholdMidpoint,
    XAPlayingDurationThresholdThirdQuartile,
    XAPlayingDurationThresholdComplete
};

@protocol XAPlayingDelegate <NSObject>

- (void)didFinishPlaying:(nonnull NSObject<XAPlaying> *)playable error:(nullable NSError *)error;

@optional

/**
 *  Provides updates on the duration of playback.
 *
 *  This is provided as a convenience so you do not have to setup your own duration monitor.
 *
 *  @param playble     The object whose duration has progressed
 *  @param currentTime The current time of the object.
 */
- (void)playable:(nonnull NSObject<XAPlaying> *)playble
     currentTime:(NSTimeInterval)currentTime
        duration:(NSTimeInterval)duration;

/**
 *  Called when the playable starts.
 *
 *  @param playable The playable that started.
 */
- (void)startedPlayback:(nonnull NSObject<XAPlaying> *)playable;

- (void)pausedPlayback:(nonnull NSObject<XAPlaying> *)playable;

- (void)resumedPlayback:(nonnull NSObject<XAPlaying> *)playable;

- (void)passedDurationThreshold:(XAPlayingDurationThreshold)threshold;

@end
