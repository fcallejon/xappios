#!/bin/sh

#usage in xcode buildphases
#./XAPP/xapp.sh -api=apikey -app=appkey

#get arguments apikey, and appkey
for i in "$@"
do
case $i in
    -api=*|--prefix=*)
    APIKEY="${i#*=}"

    ;;
    -app=*|--searchpath=*)
    APPKEY="${i#*=}"
    ;;
    --default)
    DEFAULT=YES
    ;;
    *)
            # unknown option
    ;;
esac
done
echo APIKEY = ${APIKEY}
echo APPKEY = ${APPKEY}
echo DEFAULT = ${DEFAULT}

#check if xapp key exists
myCurrentKey=$(/usr/libexec/PlistBuddy  -c "Print :XAPP" "$INFOPLIST_FILE" 2>&1)
#if not add the xapp dict, else update the values
if [[ "$myCurrentKey" == *"Does Not Exist"* ]]; then
 /usr/libexec/PlistBuddy -c "Add :XAPP:apiKey string $APIKEY" "$INFOPLIST_FILE"
 /usr/libexec/PlistBuddy -c "Add :XAPP:appKey string $APPKEY" "$INFOPLIST_FILE"
else
 /usr/libexec/PlistBuddy -c "Set :XAPP:apiKey  $APIKEY" "$INFOPLIST_FILE"
 /usr/libexec/PlistBuddy -c "Set :XAPP:appKey  $APPKEY" "$INFOPLIST_FILE"
fi
