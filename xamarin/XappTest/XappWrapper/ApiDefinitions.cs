using Foundation;
using ObjCRuntime;

namespace XappWrapper
{
	// @interface XappWrapper : NSObject
	[BaseType (typeof(NSObject))]
	interface XappWrapper
	{
		// @property (nonatomic, weak) int * ad;
		[Export ("ad", ArgumentSemantic.Weak)]
		unsafe int* Ad { get; set; }

		// @property (nonatomic, weak) int * userData;
		[Export ("userData", ArgumentSemantic.Weak)]
		unsafe int* UserData { get; set; }

		// -(void)start:(NSString *)apiKey withAppKey:(NSString *)appKey;
		[Export ("start:withAppKey:")]
		void Start (string apiKey, string appKey);

		// -(void)playInterstitial:(id)controller;
		[Export ("playInterstitial:")]
		void PlayInterstitial (NSObject controller);
	}

	// @interface wrapper : NSObject
	[BaseType (typeof(NSObject))]
	interface wrapper
	{
		// -(void)playInterstitial:(id)controller;
		[Export ("playInterstitial:")]
		void PlayInterstitial (NSObject controller);
	}
}
