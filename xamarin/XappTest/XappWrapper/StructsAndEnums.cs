using System;
using ObjCRuntime;

namespace XappWrapper
{
	[Native]
	public enum XADuration : nuint
	{
		Unspecified = 0,
		TenSecond = 10,
		FifteenSecond = 15,
		TwentySecond = 20,
		ThirtySecond = 30,
		SixtySecond = 60
	}

	[Native]
	public enum XAResultCancellationType : nint
	{
		None,
		Programmatic,
		CancelTouched,
		RemoteControlEvent
	}

	[Native]
	public enum XALogLevel : nuint
	{
		Debug = 0,
		Info,
		Warn,
		Error
	}
}
