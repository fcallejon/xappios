﻿using System;
using System.Threading;
using UIKit;
using AVFoundation;
using Foundation;

namespace XappTest
{
	public partial class ViewController : UIViewController
	{
		private binding.XappWrapper c;
		private Timer isReadyTimer;

		protected ViewController(IntPtr handle) : base(handle)
		{
			AVAudioSession.SharedInstance().Init();
			NSError error =  AVAudioSession.SharedInstance().SetCategory(
				AVAudioSessionCategory.Playback, 
				AVAudioSessionCategoryOptions.MixWithOthers);

			if (error != null)
			{
				throw new ApplicationException(error.Description);
			}

			error = AVAudioSession.SharedInstance().SetActive(true);
			if (error != null)
			{
				throw new ApplicationException(error.Description);
			}

			c = new binding.XappWrapper();
			c.Start("b454146b-0b0f-4f16-91d5-9637ccddca10", "22e1c6db-94a7-4348-a3c4-c3c562f27860", this);
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			// Perform any additional setup after loading the view, typically from a nib.

			c.GetNewAd();
			isReadyTimer = new Timer(HandleTimerCallback, null, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1));
		}

		void HandleTimerCallback(object state)
		{
			if (c.IsAdReady)
			{
				isReadyTimer.Change(Timeout.Infinite, Timeout.Infinite);
				InvokeOnMainThread(() =>
				{
					c.PlayInterstitial(this);
				});
			}
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}
	}
}
