#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <XAPP/XAPP.h>
#import <XAPP/XAPermission.h>
#import <UIKit/UIKit.h>

@interface XappWrapper : NSObject

@property (weak, nonatomic) XAAdvertisement *ad;
@property (weak, nonatomic) XAUserData *userData;
@property (nonatomic) BOOL isReady;
@property (nonatomic) BOOL isAdReady;

-(void) start:(NSString*)apiKey withAppKey:(NSString*)appKey fromViewController:(UIViewController*)controller;
-(void) getNewAd;
-(void) playInterstitial:(UIViewController*)controller;

@end
