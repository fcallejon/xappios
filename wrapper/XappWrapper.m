#import "XappWrapper.h"

@implementation XappWrapper

-(void) start:(NSString*)apiKey withAppKey:(NSString*)appKey fromViewController:(UIViewController*)controller{
    /*NSError *audioSessionError;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&audioSessionError];
    [[AVAudioSession sharedInstance] setActive:YES error:&audioSessionError];
    
    if (audioSessionError) {
        NSLog(@"Error setting up audio session: %@", audioSessionError.localizedDescription);
    }*/
    
    [XAPermission useAPIKey:apiKey applicationKey:appKey];
    [XAPermission fromViewController:controller requestRecordPermission:^(BOOL granted) {
        NSLog(@"granted: %d", granted);
    }];
    
    NSLog(@"started!");
    
    return;
}

- (void) getNewAd {
    NSLog(@"creating request");
    XARequest *request = [[XARequest alloc] init];
    NSLog(@"request initialized ... requesting ad");

    self.isAdReady = NO;
    
    [XAPP requestAd:request success:^(XAAdvertisement *advertisement) {
        //NSLog(@"Request returned %@", advertisement.adTitle);
        self.ad = advertisement;
        self.isAdReady = YES;
    } failure:^(NSError *error) {
        NSLog(@"ERROR!!!");
        //NSLog(@"Request failed %@", error ? [NSString stringWithFormat:@"with error: %@", error.localizedDescription] : @"");
    }];
    NSLog(@"waiting for ad to be ready...");
}

-(void) playInterstitial:(UIViewController*)controller {
    
    NSLog(@"6");
    
    //UIViewController* dummyController = [[UIViewController alloc] init];
    //NSLog(@"before controller");
    //dummyController.view = controller.view;
    
    [XAPP playAdAsInterstitial:self.ad
        withRootViewController:controller
               startedPlayback:^(XAAdvertisement *advertisement) {
                   NSLog(@"INICIO");
                   
               } success:^(XAAdvertisement *spot, XAResult *result) {
                   NSLog(@"TERMINO");
               } failure:^(XAAdvertisement *spot, NSError *error) {
                   NSLog(@"CON ERROR");
               }];
    NSLog(@"fin");
    
}

@end
